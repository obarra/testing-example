package com.obarra.testingexample;

import com.obarra.testingexample.repository.PaymentTermRepository;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class TestingExampleApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private PaymentTermRepository paymentTermRepository;

	@Test
	@Tag("AcceptanceTesting")
	void updatePaymentTermsOfCreditCard() throws Exception {
		MockHttpServletRequestBuilder builder =
				MockMvcRequestBuilders.put("/creditCard")
						.contentType(MediaType.APPLICATION_JSON_VALUE)
						.accept(MediaType.APPLICATION_JSON_VALUE)
						.characterEncoding("UTF-8")
						.content(getBodyJson());

		mockMvc.perform(builder)
				.andExpect(status().isOk());

		validateUpdate();
	}

	private void validateUpdate() {
		//get payments by party and check type of currency
	}

	private String getBodyJson() {
		return "{\"ownerId\":2, \"newCurrencyId\":2000}";
	}

}
