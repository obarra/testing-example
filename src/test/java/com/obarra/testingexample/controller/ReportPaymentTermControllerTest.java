package com.obarra.testingexample.controller;

import com.obarra.testingexample.dto.ReportPaymentTermDTO;
import com.obarra.testingexample.service.ReportPaymentTermService;
import com.obarra.testingexample.service.impl.ReportPaymentTermServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

class ReportPaymentTermControllerTest {

    private ReportPaymentTermService reportPaymentTermService;

    @BeforeEach
    void setUp() {
        reportPaymentTermService = mock(ReportPaymentTermServiceImpl.class);

        ReportPaymentTermDTO reportPaymentTermDTO = new ReportPaymentTermDTO();
        reportPaymentTermDTO.setCurrencyDescription("DOLARES ESTADOUNIDENSES");
        reportPaymentTermDTO.setFistNameOwner("MyName");
        reportPaymentTermDTO.setLastNameOwner("MyLastName");
        List<ReportPaymentTermDTO> reportPaymentTermDTOs = Arrays.asList(reportPaymentTermDTO);
        doReturn(reportPaymentTermDTOs).when(reportPaymentTermService).generateReportCreditCard();
    }


    @Test
    void findReportWhenPaymentTypeIsCreditShouldReturnPaymentTermsOfCreditCard() {
        //GIVEN
        ReportPaymentTermController reportPaymentTermController =
                new ReportPaymentTermController(reportPaymentTermService);
        ReportPaymentTermDTO reportPaymentTermDTO = new ReportPaymentTermDTO();
        reportPaymentTermDTO.setCurrencyDescription("DOLARES ESTADOUNIDENSES");
        reportPaymentTermDTO.setFistNameOwner("MyName");
        reportPaymentTermDTO.setLastNameOwner("MyLastName");
        List<ReportPaymentTermDTO> expected = Arrays.asList(reportPaymentTermDTO);

        //WHEN
        List<ReportPaymentTermDTO> result = reportPaymentTermController.findReport("credit");

        //THEN
        Assertions.assertIterableEquals(expected, result);
    }

    @Test
    void findReportWhenPaymentTypeIsNullShouldBeThrowNullPointerException() {
        ReportPaymentTermController reportPaymentTermController = new ReportPaymentTermController(reportPaymentTermService);

        Assertions.assertThrows(NullPointerException.class,
                () -> reportPaymentTermController.findReport(null));
    }
}