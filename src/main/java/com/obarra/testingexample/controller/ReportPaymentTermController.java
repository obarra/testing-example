package com.obarra.testingexample.controller;

import com.obarra.testingexample.dto.ReportPaymentTermDTO;
import com.obarra.testingexample.service.ReportPaymentTermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReportPaymentTermController {
    private final ReportPaymentTermService reportPaymentTermService;

    @Autowired
    public ReportPaymentTermController(final ReportPaymentTermService reportPaymentTermService) {
        this.reportPaymentTermService = reportPaymentTermService;
    }

    @GetMapping("/report")
    public List<ReportPaymentTermDTO> findReport(final @RequestParam("paymentType") String paymentType) {
        if ("credit".equals(paymentType)) {
            return reportPaymentTermService.generateReportCreditCard();
        }

        return reportPaymentTermService.generateReportDebitCard();
    }
}
