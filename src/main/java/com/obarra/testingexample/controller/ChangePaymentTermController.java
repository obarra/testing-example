package com.obarra.testingexample.controller;

import com.obarra.testingexample.dto.ChangePaymentTermDTO;
import com.obarra.testingexample.service.ChangePaymentTermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChangePaymentTermController {
    private final ChangePaymentTermService changePaymentTermService;

    @Autowired
    public ChangePaymentTermController(final ChangePaymentTermService changePaymentTermService) {
        this.changePaymentTermService = changePaymentTermService;
    }

    @PutMapping("/creditCard")
    public void update(@RequestBody ChangePaymentTermDTO changePaymentTermDTO) {
        changePaymentTermService.changeCurrencyCreditCard(changePaymentTermDTO.getNewCurrencyId().longValue(),
                changePaymentTermDTO.getOwnerId().longValue());
    }
}
