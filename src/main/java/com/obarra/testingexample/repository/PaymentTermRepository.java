package com.obarra.testingexample.repository;


import com.obarra.testingexample.model.entity.PaymentTerm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentTermRepository extends JpaRepository<PaymentTerm, Long> {
    @Query("select pt from PaymentTerm pt where pt.party.partyId = :partyId")
    List<PaymentTerm> findByPartyId(@Param("partyId") Long partyId);
}
