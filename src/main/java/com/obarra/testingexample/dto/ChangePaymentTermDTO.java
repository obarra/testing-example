package com.obarra.testingexample.dto;

public class ChangePaymentTermDTO {
    private Integer ownerId;
    private Integer newCurrencyId;

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public Integer getNewCurrencyId() {
        return newCurrencyId;
    }

    public void setNewCurrencyId(Integer newCurrencyId) {
        this.newCurrencyId = newCurrencyId;
    }
}
