package com.obarra.testingexample.dto;


import java.util.Objects;

public class ReportPaymentTermDTO {
    private String currencyDescription;
    private String fistNameOwner;
    private String lastNameOwner;

    public String getCurrencyDescription() {
        return currencyDescription;
    }

    public void setCurrencyDescription(String currencyDescription) {
        this.currencyDescription = currencyDescription;
    }

    public String getFistNameOwner() {
        return fistNameOwner;
    }

    public void setFistNameOwner(String fistNameOwner) {
        this.fistNameOwner = fistNameOwner;
    }

    public String getLastNameOwner() {
        return lastNameOwner;
    }

    public void setLastNameOwner(String lastNameOwner) {
        this.lastNameOwner = lastNameOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportPaymentTermDTO that = (ReportPaymentTermDTO) o;
        return Objects.equals(currencyDescription, that.currencyDescription) &&
                Objects.equals(fistNameOwner, that.fistNameOwner) &&
                Objects.equals(lastNameOwner, that.lastNameOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyDescription, fistNameOwner, lastNameOwner);
    }

    @Override
    public String toString() {
        return "PaymentTermDTO{" +
                "currencyDescription='" + currencyDescription + '\'' +
                ", fistNameOwner='" + fistNameOwner + '\'' +
                ", lastNameOwner='" + lastNameOwner + '\'' +
                '}';
    }
}
