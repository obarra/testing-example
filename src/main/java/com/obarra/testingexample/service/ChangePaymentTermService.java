package com.obarra.testingexample.service;

public interface ChangePaymentTermService {
    void changeCurrencyCreditCard(Long currencyId, Long partyId);
}
