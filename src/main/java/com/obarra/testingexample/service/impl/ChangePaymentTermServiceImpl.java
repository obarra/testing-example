package com.obarra.testingexample.service.impl;

import com.obarra.testingexample.model.entity.Currency;
import com.obarra.testingexample.model.entity.PaymentTerm;
import com.obarra.testingexample.repository.PaymentTermRepository;
import com.obarra.testingexample.service.ChangePaymentTermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ChangePaymentTermServiceImpl implements ChangePaymentTermService {
    private final PaymentTermRepository paymentTermRepository;

    @Autowired
    public ChangePaymentTermServiceImpl(final PaymentTermRepository paymentTermRepository) {
        this.paymentTermRepository = paymentTermRepository;
    }

    @Transactional
    @Override
    public void changeCurrencyCreditCard(Long newCurrencyId, Long partyId) {
        final List<PaymentTerm> paymentTerms = paymentTermRepository.findByPartyId(partyId);

        paymentTerms.stream()
                .filter(this::filterCreditCard)
                .forEach(p -> updatePaymentTerm(p, newCurrencyId));
    }

    private boolean filterCreditCard(PaymentTerm paymentTerm) {
        if (paymentTerm.getPaymentTypeId() == 2000) {
            return true;
        }
        return false;
    }

    private void updatePaymentTerm(PaymentTerm paymentTerm, Long newCurrencyId) {
        Currency currency = new Currency();
        currency.setCurrencyId(newCurrencyId);
        paymentTerm.setCurrency(currency);
        paymentTermRepository.save(paymentTerm);
    }
}
