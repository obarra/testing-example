package com.obarra.testingexample.service.impl;

import com.obarra.testingexample.dto.ReportPaymentTermDTO;
import com.obarra.testingexample.model.entity.PaymentTerm;
import com.obarra.testingexample.repository.PartyRepository;
import com.obarra.testingexample.repository.PaymentTermRepository;
import com.obarra.testingexample.service.ReportPaymentTermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReportPaymentTermServiceImpl implements ReportPaymentTermService {
    private final PaymentTermRepository paymentTermRepository;
    private final PartyRepository partyRepository;

    @Autowired
    public ReportPaymentTermServiceImpl(final PaymentTermRepository paymentTermRepository,
                                        final PartyRepository partyRepository) {
        this.paymentTermRepository = paymentTermRepository;
        this.partyRepository = partyRepository;
    }

    @Override
    public List<ReportPaymentTermDTO> generateReportCreditCard() {
       return  paymentTermRepository.findAll().stream()
                .filter(this::filterCreditCard)
                .map(this::mapToReport)
                .collect(Collectors.toList());
    }

    private ReportPaymentTermDTO mapToReport(PaymentTerm paymentTerm) {
        ReportPaymentTermDTO reportPaymentTermDTO = new ReportPaymentTermDTO();
        reportPaymentTermDTO.setCurrencyDescription(paymentTerm.getCurrency().getDescription());
        reportPaymentTermDTO.setFistNameOwner(paymentTerm.getParty().getFirstName());
        reportPaymentTermDTO.setLastNameOwner(paymentTerm.getParty().getLastName());
        return reportPaymentTermDTO;
    }

    private boolean filterCreditCard(PaymentTerm paymentTerm) {
        return paymentTerm.getPaymentTypeId() == 2000;
    }

    @Override
    public List<ReportPaymentTermDTO> generateReportDebitCard() {
        return null;
    }
}
