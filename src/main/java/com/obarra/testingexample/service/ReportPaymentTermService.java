package com.obarra.testingexample.service;

import com.obarra.testingexample.dto.ReportPaymentTermDTO;

import java.util.List;

public interface ReportPaymentTermService {
    List<ReportPaymentTermDTO> generateReportCreditCard();
    List<ReportPaymentTermDTO> generateReportDebitCard();
}
